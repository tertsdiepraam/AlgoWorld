import Data.List (insert)

sort :: Ord a => [a] -> [a]
sort = foldr insert []
